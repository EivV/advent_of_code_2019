package main

import (
	"fmt"
	"strconv"
)

func partOne(input []int) (int, error) {
	goodCount := 0

	var hasDouble bool
	var twoDigits string
	var isBad bool

	for i := input[0]; i <= input[1]; i++ {
		digits := strconv.Itoa(i)

		isBad = false
		hasDouble = false
		for index := 0; index < len(digits)-1; index++ {
			twoDigits = digits[index : index+2]
			d1, _ := strconv.Atoi(string(twoDigits[0]))
			d2, _ := strconv.Atoi(string(twoDigits[1]))
			if d1 > d2 {
				isBad = true
				break
			}
			if d1 == d2 {
				hasDouble = true
			}
		}
		if isBad != true && hasDouble {
			goodCount++
		}
	}

	return goodCount, nil
}

func countNums(input string) map[rune]uint8 {
	agg := map[rune]uint8{0: 0, 1: 0, 2: 0, 3: 0, 4: 0, 5: 0, 6: 0, 7: 0, 8: 0, 9: 0}
	for _, numStr := range input {
		agg[numStr] += 1
	}
	return agg
}

func checkHasDouble(digitsAsString string) bool {
	hasDouble := false
	digitsCount := countNums(digitsAsString)
	for _, count := range digitsCount {
		if count == 2 {
			hasDouble = true
		}
	}
	return hasDouble
}

func partTwo(input []int) (int, error) {
	goodCount := 0

	var twoDigits string
	var isBad bool

	for i := input[0]; i <= input[1]; i++ {
		digitsAsString := strconv.Itoa(i)
		isBad = false
		for cursor := 0; cursor < len(digitsAsString)-1; cursor++ {
			twoDigits = digitsAsString[cursor : cursor+2]
			d1, _ := strconv.Atoi(string(twoDigits[0]))
			d2, _ := strconv.Atoi(string(twoDigits[1]))
			if d1 > d2 {
				isBad = true
				break
			}
		}

		if isBad != true && checkHasDouble(digitsAsString) {
			goodCount++
		}
	}

	return goodCount, nil
}

func main() {
	input := []int{136760, 595730}

	//res, _ := partOne(input) // 1873
	res, _ := partTwo(input) // 1264
	fmt.Println("Final res:", res)
}
