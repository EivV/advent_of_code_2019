package main

import (
	"fmt"
	"math"
	"path/filepath"
	"strconv"
	"utils"
)

type Move struct {
	direction rune
	distance  uint16
}

type Point struct {
	x int
	y int
}

type Content struct {
	wires map[int][]int
}

func (c Content) addWire(wireNo, length int) {
	c.wires[wireNo] = append(c.wires[wireNo], length)
}

func (c Content) numWires() int {
	return len(c.wires)
}

func partOne(input [][]string) (int, error) {

	theMap := make(map[Point]uint8)
	start := [2]int{0, 0}

	var xOrY, goDir int

	for lineNo, line := range input {
		var bit uint8
		bit = 1 << lineNo
		current := start
		for _, moveStr := range line {
			distance, _ := strconv.Atoi(moveStr[1:])
			move := Move{
				direction: rune(moveStr[0]),
				distance:  uint16(distance),
			}
			switch move.direction {
			case 'U':
				xOrY = 1
				goDir = 1

			case 'D':
				xOrY = 1
				goDir = -1

			case 'L':
				xOrY = 0
				goDir = -1

			case 'R':
				xOrY = 0
				goDir = 1

			default:
				panic("Unknown direction.")
			}

			for i := 0; i < int(move.distance); i++ {
				current[xOrY] += goDir
				theMap[Point{current[0], current[1]}] |= bit
			}
		}
	}

	intersections := make(map[Point]int)
	for point, val := range theMap {
		if val == 3 { // for 2 lines, b'11')
			fmt.Printf("INTERSECT@ %v: %b\n", point, int64(val))
			intersections[point] = 1
		}
	}

	fmt.Println(intersections)

	closest := 10000

	for point, _ := range intersections {
		diffX := int(math.Abs(float64(start[0] - point.x)))
		diffY := int(math.Abs(float64(start[1] - point.y)))
		toCenter := diffX + diffY
		fmt.Printf("Distance: %v: %d\n", point, toCenter)
		if closest > toCenter {
			closest = toCenter
		}
	}

	return closest, nil
}

func partTwo(input [][]string) (int, error) {

	theMap := make(map[Point]Content)
	start := [2]int{0, 0}

	var xOrY, goDir int

	for lineNo, line := range input {
		current := start
		lineLength := 0
		for _, moveStr := range line {
			distance, _ := strconv.Atoi(moveStr[1:])
			move := Move{
				direction: rune(moveStr[0]),
				distance:  uint16(distance),
			}
			switch move.direction {
			case 'U':
				xOrY = 1
				goDir = 1

			case 'D':
				xOrY = 1
				goDir = -1

			case 'L':
				xOrY = 0
				goDir = -1

			case 'R':
				xOrY = 0
				goDir = 1

			default:
				panic("Unknown direction.")
			}

			for i := 0; i < int(move.distance); i++ {
				lineLength += 1
				current[xOrY] += goDir
				currentPoint := Point{current[0], current[1]}
				_, ok := theMap[currentPoint]
				if ok == false {
					theMap[currentPoint] = Content{wires: map[int][]int{}}
				}
				theMap[currentPoint].addWire(lineNo, lineLength)
			}
		}
	}

	intersections := make(map[Point]Content)
	for point, content := range theMap {

		if content.numWires() > 1 { // For 2 or more lines connecting.
			fmt.Printf("INTERSECT@ %v: %v\n", point, content)
			intersections[point] = content
		}
	}

	fmt.Println(intersections)

	closest := 10000000

	for point, content := range intersections {
		wires := content.wires
		var minLengths []int
		for _, lengths := range wires {
			min, _ := utils.MinMax(lengths)
			minLengths = append(minLengths, min)
		}
		toCenter := utils.Sum(minLengths)
		fmt.Printf("Distance: %v: %d\n", point, toCenter)
		if closest > toCenter {
			closest = toCenter
		}
	}

	return closest, nil
}

func main() {
	absPath, _ := filepath.Abs("./src/03/in.txt")
	input, err := utils.ReadLinesArrayString(absPath)
	//fmt.Println(input)

	if err != nil {
		fmt.Print(err)
	}

	//res, err := partOne(input) // 627
	res, err := partTwo(input) // 13190
	fmt.Println("Final res:", res)
}
