package utils

type Queue struct {
	container []int
}

func (q *Queue) AddMany(input []int) {
	for _, item := range input {
		q.Add(item)
	}
}

func (q *Queue) Add(item int) {
	if q.container == nil {
		q.container = []int{}
	}
	q.container = append(q.container, item)
}

func (q *Queue) Get() (res int) {
	res = q.container[0]
	q.container = q.container[1:]
	return
}
