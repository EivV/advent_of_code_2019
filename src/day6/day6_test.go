package day6

import "testing"

func TestEmptyInput(t *testing.T) {
	m := createMapFromInput([]string{})
	if m.planets != nil {
		t.Errorf("Nothing should have been added to the map")
	}
}

func Test1(t *testing.T) {
	input := []string{"COM)B", "B)C"}
	m := createMapFromInput(input)
	if m.planets == nil {
		t.Errorf("Nothing added to the map")
	}

	for _, name := range []string{"COM", "B", "C"} {
		_, ok := m.planets[name]
		if !ok {
			t.Errorf("%v missing in the map", name)
		}
	}

	pCOM := m.planets["COM"]
	if pCOM.parent != nil {
		t.Errorf("COM should have no parents")
	}

	pBcom := pCOM.children["B"]
	pB := m.planets["B"]
	if pBcom != pB {
		t.Errorf("B from COM children should be the B in map")
	}

	if pB.parent != pCOM {
		t.Errorf("B's parent should be COM")
	}

}

func TestExamplePartOne(t *testing.T) {
	input := []string{"COM)B", "B)C", "C)D", "D)E", "E)F", "B)G", "G)H", "D)I", "E)J", "J)K", "K)L"}

	res, _ := partOne(input)

	if res != 42 {
		t.Errorf("Day 1 example answer should be 42, is %d", res)
	}
}

func TestExamplePartTwo(t *testing.T) {
	input := []string{"COM)BBB", "BBB)CCC", "CCC)DDD", "DDD)EEE", "EEE)FFF", "BBB)GGG", "GGG)HHH", "DDD)III", "EEE)JJJ", "JJJ)KKK", "KKK)LLL", "KKK)YOU", "III)SAN"}

	res, _ := partTwo(input)

	if res != 4 {
		t.Errorf("Day 2 example answer should be 4, is %d", res)
	}
}
