package day6

import (
	"fmt"
	"path/filepath"
	"strings"
	"utils"
)

type Planet struct {
	name     string
	parent   *Planet
	children map[string]*Planet
}

func (p *Planet) addChild(child *Planet) {
	if p.children == nil {
		(*p).children = map[string]*Planet{}
	}
	_, childExists := p.children[child.name]
	if childExists == false {
		p.children[child.name] = child
		child.parent = p
	}
}

type Map struct {
	planets map[string]*Planet
}

func (m *Map) addPlanet(planet *Planet) {
	planetInMap, weHaveThePlanetAlready := m.planets[planet.name]

	if !weHaveThePlanetAlready {
		m.planets[planet.name] = planet
	} else {
		if planetInMap.parent == nil {
			planetInMap.parent = planet.parent
		}
		planet = planetInMap
	}

	if planet.parent != nil {
		parentInMap, weHaveParentPlanetAlready := m.planets[planet.parent.name]
		if !weHaveParentPlanetAlready {
			m.addPlanet(planet.parent)
		} else {
			parentInMap.addChild(planet)
		}
	}
}

func (m *Map) print() {
	for _, planet := range m.planets {
		fmt.Println(planet)
	}
}

func (m *Map) leaves() (res []*Planet) {
	res = []*Planet{}

	for _, planet := range m.planets {
		if planet.children == nil {
			res = append(res, planet)
		}
	}
	return
}

func (m *Map) distanceToRoot(leaf *Planet) (distance int, path string) {
	path = ""
	distance = 0
	parent := leaf.parent
	path += leaf.name
	for parent != nil {
		distance += 1
		path += parent.name
		parent = parent.parent
	}
	return
}

func (m *Map) addPlanetFromPlanetString(planetString string) {
	s := strings.Split(planetString, ")")
	planetName := s[1]
	planetParentName := s[0]
	parent := &Planet{name: planetParentName}
	planet := &Planet{name: planetName, parent: parent}
	parent.addChild(planet)
	m.addPlanet(planet)
}

func createMapFromInput(input []string) (m Map) {
	if len(input) > 0 {
		m.planets = map[string]*Planet{}
	}
	for _, planetString := range input {
		m.addPlanetFromPlanetString(planetString)
	}
	return m
}

func (m *Map) calculateMapChecksum() (int, error) {
	s := 0
	var current *Planet
	paths := map[string]bool{}
	for _, leaf := range m.leaves() {
		current = leaf
		dist, path := m.distanceToRoot(current)
		paths[path] = true
		s += dist
		for current.parent != nil {
			current = current.parent
			dist, path := m.distanceToRoot(current)
			_, isIn := paths[path]
			if !isIn {
				s += dist
				paths[path] = true
			}
		}
	}
	return s, nil
}

func partOne(input []string) (int, error) {
	m := createMapFromInput(input)
	return m.calculateMapChecksum()
}

func (m *Map) findPathToSanta() (distance int, err error) {
	pSANTA := m.planets["SAN"]
	pYOU := m.planets["YOU"]

	_, pathSANTA := m.distanceToRoot(pSANTA)
	_, pathYOU := m.distanceToRoot(pYOU)

	commonPart := ""
	p1 := ""
	p2 := ""

	counter := 1
	for p1 == p2 {
		p1 = pathSANTA[len(pathSANTA)-counter : len(pathSANTA)-counter+1]
		p2 = pathYOU[len(pathYOU)-counter : len(pathYOU)-counter+1]
		if p1 == p2 {
			commonPart = pathYOU[len(pathYOU)-counter:len(pathYOU)-counter+1] + commonPart
		}
		counter++
	}

	uncommonSANTA := pathSANTA[:len(pathSANTA)-len(commonPart)]
	uncommonYOU := pathYOU[:len(pathYOU)-len(commonPart)]

	objectsSANTA := len(uncommonSANTA)/3 - 1 // -1 for the SANTA himself
	objectsYOU := len(uncommonYOU)/3 - 1

	result := objectsSANTA + objectsYOU

	return result, nil
}

func partTwo(input []string) (int, error) {
	m := createMapFromInput(input)
	return m.findPathToSanta()
}

func Run() {
	absPath, _ := filepath.Abs("./src/day6/in.txt")
	input, err := utils.ReadLinesString(absPath)
	// fmt.Println(input)

	if err != nil {
		fmt.Print(err)
	}

	//res, err := partOne(input) // 151345
	res, err := partTwo(input) // 391
	fmt.Println("Final res:", res)
}
