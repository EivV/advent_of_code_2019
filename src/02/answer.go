package main

import (
	"computer"
	"fmt"
	"path/filepath"
	"utils"
)

func partOne(input []int) int {
	input[1] = 12
	input[2] = 2

	c := computer.NewComputer()
	res := c.ComputeAtZero(input, []int{})
	return res
}

func partTwo(input []int) int {
	c := computer.NewComputer()
	var stop bool
	var res int
	var i, j int
	for i = 0; i <= 99; i++ {
		if stop != true {
			for j = 0; j <= 99; j++ {
				//i = 12
				//j = 2
				process := make([]int, len(input))
				process = append(input[:0:0], input...)
				//process := input
				process[1] = i
				process[2] = j

				res = c.ComputeAtZero(process, []int{})
				fmt.Println(res)
				if res == 19690720 {
					stop = true
					fmt.Print("FOUND:")
					fmt.Print(i, j)
					return i*100 + j
				}
			}
		}
	}
	return -1
}

func main() {
	absPath, _ := filepath.Abs("./src/02/in.txt")
	input, err := utils.ReadLineArrayInt(absPath)
	//fmt.Println(input)

	if err != nil {
		fmt.Print(err)
	}

	//res := partOne(input) // 4138658
	res := partTwo(input) // 7264
	fmt.Println("Final res:", res)
}
