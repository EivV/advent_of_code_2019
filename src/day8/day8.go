package day8

import (
	"fmt"
	"path/filepath"
	"utils"
)

func splitLayers(imageString string, width, height int) []string {
	layerSize := width * height
	res := []string{}
	for i := 0; i < len(imageString); i += layerSize {
		res = append(res, imageString[i:i+layerSize])
	}
	return res
}

func countColors(layer string) map[rune]int {
	counter := map[rune]int{'0': 0, '1': 0, '2': 0, '3': 0, '4': 0, '5': 0, '6': 0, '7': 0, '8': 0, '9': 0}
	for _, color := range layer {
		counter[color] += 1
	}
	return counter
}

func partOne(imageString string, width, height int) int {
	layers := splitLayers(imageString, width, height)
	layerWithFewestZeros := map[rune]int{'0': 1000000, '1': 0, '2': 0, '3': 0, '4': 0, '5': 0, '6': 0, '7': 0, '8': 0, '9': 0}
	for _, layer := range layers {
		count := countColors(layer)
		if count['0'] < layerWithFewestZeros['0'] {
			layerWithFewestZeros = count
		}
	}
	return layerWithFewestZeros['1'] * layerWithFewestZeros['2']
}

func flattenLayers(layers []string) string {
	layerSize := len(layers[0])
	var image string
	for pixelNo := 0; pixelNo < layerSize; pixelNo++ {
		var pixel = uint8('3') // unknown color
		for layerNo := 0; layerNo < len(layers); layerNo++ {
			layerPixel := layers[layerNo][pixelNo]
			if pixel == '3' {
				pixel = layerPixel
				continue
			}
			if pixel == '2' && layerPixel < pixel {
				pixel = layerPixel
				break
			}
			if pixel < '1' {
				break
			}
		}
		image += string(rune(pixel))
	}
	return image
}

func printImage(image string, width int) {
	for i, pixel := range image {
		if i%width == 0 {
			fmt.Printf("\n%s", string(pixel))
		} else {
			fmt.Printf("%s", string(pixel))
		}
	}
}

func partTwo(imageString string, width, height int) int {
	layers := splitLayers(imageString, width, height)
	image := flattenLayers(layers)
	printImage(image, width)
	return 0
}

func Run() {
	absPath, _ := filepath.Abs("./src/day7/in.txt")
	input, err := utils.ReadLinesString(absPath)
	// fmt.Println(input)

	if err != nil {
		fmt.Print(err)
	}

	//res := partOne(input[0], 25, 6) // 2032
	res := partTwo(input[0], 25, 6) // CFCUG
	fmt.Println("Final res:", res)
}
