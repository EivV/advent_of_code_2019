package day7

import (
	"computer"
	"fmt"
	"path/filepath"
	"reflect"
	"strconv"
	"utils"
)

func computeAmp(program []int, phase, startPhase int) int {
	c := computer.NewComputer()
	c.LoadProgram(program)
	//c.debugMode = true
	return utils.LastElemInt(c.ComputeOutput([]int{phase, startPhase}))
}

func partOne(program []int) int {
	perms := utils.Permutations([]int{0, 1, 2, 3, 4})
	currentBestOutput := 0
	for _, perm := range perms {
		output := 0
		for _, phase := range perm {
			source := make([]int, len(program))
			copy(source[:], program)
			output = computeAmp(source, phase, output)
		}
		fmt.Printf("perm %v = %v\n", perm, output)
		_, max := utils.MinMax([]int{currentBestOutput, output})
		currentBestOutput = max
	}
	return currentBestOutput
}

func partTwo(program []int) int {
	//perms := utils.Permutations([]int{5, 6, 7, 8, 9})
	perms := [][]int{{5, 6, 7, 8, 9}} // ex 1
	//perms := [][]int{{9,7,8,5,6}}  //ex 2
	currentBestOutput := 0
	for _, perm := range perms {
		output := 0
		channels := make([]chan int, 0)
		computers := make([]computer.Computer, 0)
		for i := 0; i < len(perm); i++ {
			source := make([]int, len(program))
			copy(source[:], program)

			c := computer.NewComputer()
			c.Debug = true
			c.LoadProgram(program)

			computers = append(computers, c)
			channels = append(channels, c.Input)
		}

		for _, c := range computers {
			if !reflect.DeepEqual(c.Memory, program) {
				panic("no exual")
			}
		}

		for i, phase := range perm {
			computers[i].SetName(strconv.Itoa(phase))
			go computers[i].Run()
			computers[i].Input <- phase
		}

		firstCh := channels[0]

		firstCh <- 0

		c4Done := false
		lastOutput := 0
		for !c4Done {
			select {
			case o1 := <-computers[0].Output:
				fmt.Printf("[[%s]] -> %d -> [[%s]]\n", computers[0].Name, o1, computers[1].Name)
				channels[1] <- o1
			case o2 := <-computers[1].Output:
				fmt.Printf("[[%s]] -> %d -> [[%s]]\n", computers[1].Name, o2, computers[2].Name)
				channels[2] <- o2
			case o3 := <-computers[2].Output:
				fmt.Printf("[[%s]] -> %d -> [[%s]]\n", computers[2].Name, o3, computers[3].Name)
				channels[3] <- o3
			case o4 := <-computers[3].Output:
				fmt.Printf("[[%s]] -> %d -> [[%s]]\n", computers[3].Name, o4, computers[4].Name)
				channels[4] <- o4
			case o5 := <-computers[4].Output:
				fmt.Printf("[[%s]] -> %d -> [[%s]]\n", computers[4].Name, o5, computers[0].Name)
				lastOutput = o5
				channels[0] <- o5
			case <-computers[4].Done:
				c4Done = true
			}
		}
		fmt.Printf("------------- BROKEN OUT\n")

		output = lastOutput
		fmt.Printf("perm %v = %v\n", perm, output)
		_, max := utils.MinMax([]int{currentBestOutput, output})
		currentBestOutput = max
	}
	return currentBestOutput
}

func Run() {
	absPath, _ := filepath.Abs("./src/day7/in.txt")
	input, err := utils.ReadLineArrayInt(absPath)
	// fmt.Println(input)

	if err != nil {
		fmt.Print(err)
	}

	//res := partOne(input) // 398674
	res := partTwo(input) //
	fmt.Println("Final res:", res)
}
