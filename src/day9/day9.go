package day9

import (
	"computer"
	"fmt"
	"path/filepath"
	"utils"
)

func runProgram(program, userInput []int) []int {
	c := computer.NewComputer()
	c.Debug = false
	c.LoadProgram(program)
	output := c.ComputeOutput(userInput)
	return output
}

func partOne(program []int) []int {
	return runProgram(program, []int{1})
}

func partTwo(program []int) []int {
	return runProgram(program, []int{2})
}

func Run() {
	absPath, _ := filepath.Abs("./src/day9/in.txt")
	input, err := utils.ReadLineArrayInt(absPath)
	// fmt.Println(program)

	if err != nil {
		fmt.Print(err)
	}

	//res := partOne(input) // 2775723069
	res := partTwo(input) // 49115
	fmt.Println("Final res:", res)
}
