package computer

import (
	"fmt"
	"strconv"
	"utils"
)

type Computer struct {
	Name         string
	Debug        bool
	Memory       []int
	Input        chan int
	Output       chan int
	Done         chan bool
	pointer      int
	relativeBase int
}

func NewComputer() Computer {
	return Computer{
		Debug:        false,
		pointer:      0,
		relativeBase: 0,
		Input:        make(chan int),
		Output:       make(chan int, 100),
		Done:         make(chan bool, 0),
	}
}

func (c *Computer) SetName(name string) {
	c.Name = name
}

func (c *Computer) LoadProgram(input []int) {
	c.Memory = input
}

func (c *Computer) printDebug(format string, a ...interface{}) {
	if c.Debug {
		format = fmt.Sprintf("[%s] ", c.Name) + format
		fmt.Printf(format, a...)
	}
}

func (c *Computer) growMemoryBounds(address int) {
	if address+1 > len(c.Memory) {
		additionalLength := address - len(c.Memory) + 1
		additionalMemory := make([]int, additionalLength)
		c.Memory = append(c.Memory, additionalMemory...)
	}
}

func (c *Computer) getFromMemory(address int) int {
	c.growMemoryBounds(address)
	return c.Memory[address]
}

func (c *Computer) putToMemory(address, value int) {
	c.growMemoryBounds(address)
	c.Memory[address] = value
}

func (c *Computer) getValue(cursor int, mode int) int {
	address := c.Memory[cursor]
	switch mode {
	case 0:
		/* Position mode */
		return c.getFromMemory(address)
	case 1:
		/* Immediate mode */
		return address
	case 2:
		/* Relative mode */
		relativeAddress := c.relativeBase + address
		return c.getFromMemory(relativeAddress)
	default:
		panic("Unknown mode")
	}
}

func (c *Computer) getPos(cursor int, mode int) int {
	address := c.Memory[cursor]
	switch mode {
	case 0:
		/* Position mode */
		return address
	case 2:
		/* Relative mode */
		relativeAddress := c.relativeBase + address
		return relativeAddress
	default:
		panic("Unknown getPos mode")
	}
}

func (c *Computer) splitOp(op int) (mode3, mode2, mode1, opcode int) {
	opString := strconv.Itoa(op)
	fullPaddedString := fmt.Sprintf("%05v", opString)
	mode3, _ = strconv.Atoi(string(fullPaddedString[0]))
	mode2, _ = strconv.Atoi(string(fullPaddedString[1]))
	mode1, _ = strconv.Atoi(string(fullPaddedString[2]))
	opcode, _ = strconv.Atoi(fullPaddedString[3:5])
	return
}

func (c *Computer) Run() {
	c.printDebug("Started running!\n")
	cursor := c.pointer
	var op, value1, value2, pos int

	for op != 99 {
		c.printDebug("@988: %v\n", c.getFromMemory(988))
		op = c.getFromMemory(cursor)

		mode3, mode2, mode1, opcode := c.splitOp(op)
		c.printDebug("mode: %v%v%v|%v -- ", mode3, mode2, mode1, opcode)
		address1 := cursor + 1
		address2 := cursor + 2
		address3 := cursor + 3
		switch opcode {
		case 99:
			c.printDebug("DONE\n\n")
			c.Done <- true
			close(c.Done)
			close(c.Output)
			return

		case 1:
			value1 = c.getValue(address1, mode1)
			value2 = c.getValue(address2, mode2)
			pos = c.getPos(address3, mode3)
			c.printDebug("%v: @%v = %v@%v + %v@%v\n", opcode, pos, value1, address1, value2, address2)
			c.putToMemory(pos, value1+value2)
			cursor += 4

		case 2:
			value1 = c.getValue(address1, mode1)
			value2 = c.getValue(address2, mode2)
			pos = c.getPos(address3, mode3)
			c.printDebug("%v: @%v = %v@%v * %v@%v\n", opcode, pos, value1, address1, value2, address2)
			c.putToMemory(pos, value1*value2)

			cursor += 4

		case 3:
			/* Read from c.Input. */
			pos = c.getPos(address1, mode1)
			c.printDebug("Waiting for Input\n")
			inputValue := <-c.Input
			c.printDebug("<- %v\n", inputValue)
			c.printDebug("%v: @%v = %v\n", opcode, pos, inputValue)
			c.putToMemory(pos, inputValue)
			cursor += 2

		case 4:
			value1 = c.getValue(address1, mode1)
			c.printDebug("-> %v@%v\n", value1, address1)
			c.Output <- value1
			cursor += 2

		case 5:
			value1 = c.getValue(address1, mode1)
			value2 = c.getValue(address2, mode2)
			c.printDebug("%v: %v@%v != 0?: @%v = %v\n", opcode, value1, address1, address2, value2)
			if value1 != 0 {
				cursor = value2
			} else {
				cursor += 3
			}

		case 6:
			value1 = c.getValue(address1, mode1)
			value2 = c.getValue(address2, mode2)
			c.printDebug("%v: %v@%v == 0?: @%v = %v\n", opcode, value1, address1, address2, value2)
			if value1 == 0 {
				cursor = value2
			} else {
				cursor += 3
			}

		case 7:
			value1 = c.getValue(address1, mode1)
			value2 = c.getValue(address2, mode2)
			pos = c.getPos(address3, mode3)
			c.printDebug("%v: %v@%v < %v@%v?: -> @%v=1\n", opcode, value1, address1, value2, address2, pos)
			if value1 < value2 {
				c.putToMemory(pos, 1)
			} else {
				c.putToMemory(pos, 0)
			}
			cursor += 4

		case 8:
			value1 = c.getValue(address1, mode1)
			value2 = c.getValue(address2, mode2)
			pos = c.getPos(address3, mode3)
			c.printDebug("%v: %v@%v == %v@%v?: -> @%v=1\n", opcode, value1, address1, value2, address2, pos)
			if value1 == value2 {
				c.putToMemory(pos, 1)
			} else {
				c.putToMemory(pos, 0)
			}
			cursor += 4

		case 9:
			value1 = c.getValue(address1, mode1)
			updatedRB := c.relativeBase + value1
			c.printDebug("RB%v: RB!%v=> +%v@%v = RB!%v\n", opcode, c.relativeBase, value1, address1, updatedRB)
			c.relativeBase = updatedRB
			cursor += 2

		default:
			errorString := fmt.Sprintf("Unknown opcode: %v\n", opcode)
			panic(errorString)
		}

	}
	return
}

func (c *Computer) ComputeAtZero(userInput []int) int {
	for _, value := range userInput {
		c.Input <- value
	}
	close(c.Input)
	go c.Run()
	<-c.Done
	return utils.LastElemInt(c.Memory)
}

func (c *Computer) ComputeOutput(userInput []int) []int {
	go c.Run()
	for _, value := range userInput {
		c.Input <- value
	}
	close(c.Input)
	<-c.Done

	output := make([]int, 0)
	for value := range c.Output {
		output = append(output, value)
	}

	return output
}
