package main

import (
	"computer"
	"fmt"
	"path/filepath"
	"utils"
)

func partOne(input []int) int {
	userInput := []int{1}
	c := computer.NewComputer()
	c.LoadProgram(input)
	output := c.ComputeOutput(userInput)
	return utils.LastElemInt(output)
}

func partTwo(input []int) int {
	userInput := []int{5}
	c := computer.NewComputer()
	c.LoadProgram(input)
	output := c.ComputeOutput(userInput)
	return utils.LastElemInt(output)
}

func main() {
	absPath, _ := filepath.Abs("./src/05/in.txt")
	input, err := utils.ReadLineArrayInt(absPath)
	//fmt.Println(input)

	if err != nil {
		fmt.Print(err)
	}

	//res := partOne(input) // 10987514
	res := partTwo(input) // 14195011
	fmt.Println("Final res:", res)
}
