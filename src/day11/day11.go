package day11

import (
	"computer"
	"fmt"
	"path/filepath"
	"utils"
)

func partOne(program []int) int {
	c := computer.NewComputer()
	c.LoadProgram(program)
	c.Debug = true
	go c.Run()
	c.Input <- 0

	running := true
	for running {
		select {
		case output := <-c.Output:
			fmt.Println("Got OUTPUT:", output)
			c.Input <- output
		case <-c.Done:
			fmt.Println("Got a DONE")
			running = false
		}
	}

	return -1
}

func Run() {
	absPath, _ := filepath.Abs("./src/day11/in.txt")
	input, err := utils.ReadLineArrayInt(absPath)
	// fmt.Println(program)

	if err != nil {
		fmt.Print(err)
	}

	res := partOne(input) //
	//res := partTwo(input) //
	fmt.Println("Final res:", res)
}
