package day10

import (
	"fmt"
	"path/filepath"
	"utils"
)

type GridBoundary struct {
	x, y int
}

type Point struct {
	X, Y int
}

func pointsOnLine(origin Point, deltaX, deltaY int, boundary GridBoundary) (points []Point) {
	counter := 0
	var pointX, pointY int
	x := origin.X
	y := origin.Y
	for (pointX <= boundary.x && pointY <= boundary.y) && (pointX >= 0 && pointY >= 0) {
		counter++
		pointX = x + deltaX*counter
		pointY = y + deltaY*counter
		if pointX >= 0 && pointY >= 0 {
			points = append(points, Point{pointX, pointY})
		}
	}
	return points
}

func ringCoords(center Point, level int) (points []Point) {
	x := center.X
	y := center.Y

	lineXPlus := x + level
	lineXMinus := x - level
	lineYPlus := y + level
	lineYMinus := y - level
	for i := -level; i <= level; i++ {
		for j := -level; j <= level; j++ {
			newX := x + i
			newY := y + j
			if (newX != x || newY != y) && (newX == lineXPlus || newX == lineXMinus || newY == lineYPlus || newY == lineYMinus) {
				points = append(points, Point{newX, newY})
			}
		}
	}
	return
}

func pointsTowardsPoint(origin, towards Point, boundary GridBoundary) (points []Point) {
	points = pointsOnLine(origin, towards.X-origin.X, towards.Y-origin.Y, boundary)
	return
}

func distanceToBoundary(point Point, boundary GridBoundary) (currentMax int) {
	distXOrigin := point.X
	distYOrigin := point.Y
	distXBoundary := boundary.x - point.X
	distYBoundary := boundary.y - point.Y
	distances := []int{distXOrigin, distYOrigin, distXBoundary, distYBoundary}
	for _, distance := range distances {
		if distance > currentMax {
			currentMax = distance
		}
	}
	return currentMax
}

func generatePointsFromBoundary(boundary GridBoundary) (points []Point) {
	for i := 0; i <= boundary.x; i++ {
		for j := 0; j <= boundary.y; j++ {
			points = append(points, Point{i, j})
		}
	}
	return
}

func markAsteroids(input [][]string) (points map[Point]bool) {
	points = map[Point]bool{}
	for y, line := range input {
		for x, dot := range line {
			if dot == "#" {
				points[Point{x, y}] = true
			}
		}
	}
	return
}

func markVisibleAsteroidsFromPoint(origin Point, boundary GridBoundary, asteroidCoords map[Point]bool) map[Point]bool {
	pointVisibleAsteroids := map[Point]bool{}
	pointObscureAsteroids := map[Point]bool{}
	toBoundary := distanceToBoundary(origin, boundary)
	for level := 1; level <= toBoundary; level++ {
		ringPoints := ringCoords(origin, level)
		for _, towards := range ringPoints {
			linePoints := pointsTowardsPoint(origin, towards, boundary)
			foundVisibleOnTheLine := false
			for _, linePoint := range linePoints {
				_, isAsteroid := asteroidCoords[linePoint]
				if isAsteroid {
					_, alreadyObscured := pointObscureAsteroids[linePoint]
					if !alreadyObscured {
						if foundVisibleOnTheLine {
							pointObscureAsteroids[linePoint] = true
						} else {
							_, alreadyVisible := pointVisibleAsteroids[linePoint]
							if !alreadyVisible {
								pointVisibleAsteroids[linePoint] = true
								foundVisibleOnTheLine = true
							}
						}
					}
				}
			}
		}
	}
	return pointVisibleAsteroids
}

func selectPointWithHighestVisibility(visibilityMap map[Point]map[Point]bool) (bestPoint Point, highestVisibility int) {
	highestVisibility = -1
	for point, pointMarkerMapToVisible := range visibilityMap {
		visibility := len(pointMarkerMapToVisible)
		fmt.Printf("%v @ %v\n", visibility, point)
		if visibility > highestVisibility {
			highestVisibility = visibility
			bestPoint = point
		}
	}
	return
}

func partOne(input [][]string) int {
	height := len(input) - 1
	width := len(input[0]) - 1
	boundary := GridBoundary{width, height}
	pointsInMap := generatePointsFromBoundary(boundary)

	asteroidCoords := markAsteroids(input)

	visibilityMap := map[Point]map[Point]bool{}

	for _, origin := range pointsInMap {
		if _, isAsteroid := asteroidCoords[origin]; !isAsteroid {
			continue
		}
		pointVisibleAsteroids := markVisibleAsteroidsFromPoint(origin, boundary, asteroidCoords)
		visibilityMap[origin] = pointVisibleAsteroids
	}
	printVisibilityMap(visibilityMap, boundary.y+1, boundary.x+1)

	_, highestVisibility := selectPointWithHighestVisibility(visibilityMap)
	return highestVisibility
}

func printVisibilityMap(visibilityMap map[Point]map[Point]bool, height, width int) {
	fmt.Print("\n-- START VISIBILITY MAP --\n\n")
	visibility := 0
	for i := 0; i < width; i++ {
		for j := 0; j < height; j++ {
			p := Point{j, i}
			visibility = 0
			if pointMarkers, hasVisible := visibilityMap[p]; hasVisible {
				visibility = len(pointMarkers)
			}
			if visibility == 0 {
				fmt.Print(".")
			} else {
				fmt.Print(visibility)
			}
		}
		fmt.Print("\n")
	}
	fmt.Print("\n-- END VISIBILITY MAP --\n\n")
}

func Run() {
	absPath, _ := filepath.Abs("./src/day10/in.txt")
	input, err := utils.ReadLinesArrayString(absPath, "")
	// fmt.Println(program)

	if err != nil {
		fmt.Print(err)
	}

	res := partOne(input) //
	//res := partTwo(input) //
	fmt.Println("Final res:", res)
}
