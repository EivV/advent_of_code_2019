package day10

import (
	"reflect"
	"testing"
)

func Test_pointsOnLine(t *testing.T) {
	type args struct {
		origin   Point
		deltaX   int
		deltaY   int
		boundary GridBoundary
	}
	tests := []struct {
		name string
		args args
		want []Point
	}{
		{
			name: "origin positive diagonal",
			args: args{
				origin:   Point{0, 0},
				deltaX:   1,
				deltaY:   1,
				boundary: GridBoundary{4, 4},
			},
			want: []Point{{1, 1}, {2, 2}, {3, 3}, {4, 4}},
		},
		{
			name: "origin, negative diagonal",
			args: args{
				origin:   Point{0, 0},
				deltaX:   -1,
				deltaY:   -1,
				boundary: GridBoundary{4, 4},
			},
			want: nil,
		},
		{
			name: "1,1, negative diagonal",
			args: args{
				origin:   Point{1, 1},
				deltaX:   -1,
				deltaY:   -1,
				boundary: GridBoundary{4, 4},
			},
			want: []Point{{0, 0}},
		},
		{
			name: "origin, 2-5 diagonal",
			args: args{
				origin:   Point{1, 1},
				deltaX:   2,
				deltaY:   5,
				boundary: GridBoundary{11, 11},
			},
			want: []Point{{3, 6}, {5, 11}},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := pointsOnLine(tt.args.origin, tt.args.deltaX, tt.args.deltaY, tt.args.boundary); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("pointsOnLine() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_ringCoords(t *testing.T) {
	type args struct {
		center Point
		level  int
	}
	tests := []struct {
		name       string
		args       args
		wantPoints []Point
	}{
		{
			name: "origin, level 1",
			args: args{
				center: Point{0, 0},
				level:  1,
			},
			wantPoints: []Point{
				{-1, -1},
				{-1, 0},
				{-1, 1},
				{0, -1},
				{0, 1},
				{1, -1},
				{1, 0},
				{1, 1},
			},
		},
		{
			name: "2,3, level 2",
			args: args{
				center: Point{2, 3},
				level:  2,
			},
			wantPoints: []Point{
				{0, 1}, {0, 2}, {0, 3}, {0, 4}, {0, 5}, {1, 1}, {1, 5}, {2, 1}, {2, 5}, {3, 1}, {3, 5}, {4, 1}, {4, 2}, {4, 3}, {4, 4}, {4, 5},
			},
		},
		{
			name: "0,0, level 2",
			args: args{
				center: Point{0, 0},
				level:  2,
			},
			wantPoints: []Point{
				{-2, -2},
				{-2, -1},
				{-2, 0},
				{-2, 1},
				{-2, 2},
				{-1, -2},
				{-1, 2},
				{0, -2},
				{0, 2},
				{1, -2},
				{1, 2},
				{2, -2},
				{2, -1},
				{2, 0},
				{2, 1},
				{2, 2},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if gotPoints := ringCoords(tt.args.center, tt.args.level); !reflect.DeepEqual(gotPoints, tt.wantPoints) {
				t.Errorf("ringCoords() = %v, want %v", gotPoints, tt.wantPoints)
			}
		})
	}
}

func Test_partOne(t *testing.T) {
	type args struct {
		input [][]string
	}
	tests := []struct {
		name string
		args args
		want int
	}{
		{
			name: "part 1, example 1",
			args: args{
				input: [][]string{
					{".", "#", ".", ".", "#"},
					{".", ".", ".", ".", "."},
					{"#", "#", "#", "#", "#"},
					{".", ".", ".", ".", "#"},
					{".", ".", ".", "#", "#"},
				},
			},
			//want: Point{3, 4},
			want: 8,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := partOne(tt.args.input); got != tt.want {
				t.Errorf("partOne() = %v, want %v", got, tt.want)
			}
		})
	}
}
